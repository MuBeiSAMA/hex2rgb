# Gitlab npm 包管理

<!-- 在 13.3 版本后 -->
前段时间公司私有化部署的 gitlab 更新了下，发现 gitlab 支持 npm 的包管理了

随即打算把私服 nexus 上的 npm 包迁移一下，于是有了这篇文章

这篇文章会以 gitlab 官网为示例逐步介绍如何在 gitlab 中发布和使用自己的 npm 包

除 npm 以外，gitlab 还支持大多数常见的包管理器，如 Go、Maven、Ruby Gems 等

**前提条件：**

- node & npm/yarn
- gitlab 账号

## gitlab npm 包注册表

gitlab 要求注册的包必须是 [scope packages](https://docs.npmjs.com/cli/v7/using-npm/scope/)，即包名称的格式必须是 `@scope/package-name`

gitlab 提供两个选项来作为包的端点，项目级和实例级：

- **项目级**：当你有很少的 npm 包并且它们不在同一个 GitLab 组中时使用。该软件包的命名约定没有在这个级别执行。相反，您应该为您的包使用范围。使用范围时，注册表 URL仅针对该范围更新。
在项目级别注册包时，您可以使用/附加您自己的唯一范围到您的包名称。
如果需要，可以公开在项目级别注册的包
- **实例级**：当您在不同的 GitLab 组或它们自己的命名空间中有许多 npm 包时使用。请务必遵守包命名约定

这是谷歌翻译的原文，不太好理解，后面会解释两种端点的差异

这块详情可见[官方文档](https://docs.gitlab.com/ee/user/packages/npm_registry/#use-the-gitlab-endpoint-for-npm-packages)

## gitlab 设置

先在 gitlab 中创建一个空白仓库 `hex2rgb` 作为我们待会项目提交的仓库

![图 1](https://i.loli.net/2021/11/05/K7MztLoR3e6acSq.png)  

![图 2](https://i.loli.net/2021/11/05/TDnLeW8ihaZNozy.png)  

创建好后记录一下存储库的 git URL 和 project ID, 后面会用到它们

## 初始化项目

接下来写一个简单的 commonJS 模块作为待会要发布的 npm 包

创建并进入文件夹

```sh
mkdir hex2rgb && cd hex2rgb
```

项目初始化，包名称需要注意使用 scope 包模式（`@username/projectname`），scope 为自己的用户名，其他默认

```sh
yarn init

# yarn init v1.22.4
# question name (hex2rgb): @mubeisama/hex2rgb
```

初始化的 package.json 内容如下：

```json
{
  "name": "@mubeisama/hex2rgb",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT"
}

```

添加 `/index.js` 文件

```js
/**
 * @param {string} hex example: #000000
 * @return {string}
 */
function hex2rgb(hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  if (!result) return null
  const [, r, g, b] = result
  return `rbg(${parseInt(r, 16)}, ${parseInt(g, 16)}, ${parseInt(b, 16)})`
}

module.exports = hex2rgb
```

写好了，准备发布

## 发布包

gitlab 提供两种发布方法：

- 命令行，`npm publish` 或 `yarn publish`，这种方式需要手动添加个人的 gitlab `Access Token` 到配置中
- CI/CD，触发后自动发布，不需要手动配置 `Access Token`

### CI/CD 方式

添加 `/.npmrc` 文件

```sh
# Set URL for your scope packages.
# @<your_scope>:registry=https://gitlab.com/api/v4/npm/
@mubeisama:registry=https://gitlab.com/api/v4/npm/

# Add the token for the scope packages URL. This will allow you to download
//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}

# Add token for uploading to the registry. Replace <your_project_id>
# //gitlab.com/api/v4/projects/<your_project_id>/packages/npm/:_authToken=${CI_JOB_TOKEN}

//gitlab.com/api/v4/projects/31255575/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

内容为鉴权配置，注意替换 scope 名称 和 project_id

此处的 `CI_JOB_TOKEN` 是 CI/CD 的默认环境变量，可以代替个人 Token

再添加 `/.gitlab-ci.yml` 文件

```yml
image: node:14.18.1-slim

stages:
  - build

build:
  stage: build
  script:
    - npm publish
  only:
    - main
```

更新 `package.json`，增加 publishConfig

```json
{
  "name": "@mubeisama/hex2rgb",
  ...
  "publishConfig": {
    // 替换为自己的 scope
    "@mubeisama:registry": "https://gitlab.com/api/v4/projects/31255575/packages/npm/"
  }
}
```

配置完成。接下来提交到 main 分支就会触发 CI/CD 了。

```sh
# 替换为自己的仓库地址
git init --initial-branch=main
git remote add origin https://gitlab.com/MuBeiSAMA/hex2rgb.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

如果 `git init --initial-branch=main` 报错的话，升级 `git` 到最新版即可解决

配置没错的话，CI/CD 的 pipeline 里会有条记录

![图 1](https://i.loli.net/2021/11/12/4iHXDJNepsWnzdj.png)  

跑完后包注册表里就能看到打好的包了

![图 2](https://i.loli.net/2021/11/12/jeZWIqOyT1fihEG.png)  

### 命令行方式

在 gitlab 首页点右上角的头像，展开的菜单里点 preferences 进入偏好设置

在 Access Tokens 面板创建 Token，权限按需勾选，创建好后的 Token 只会在顶部出现一次，记录下来

![图 3](https://i.loli.net/2021/11/12/eBFOsavKmCpQXul.png)  

回到项目里，修改 `.npmrc`，将 CI_JOB_TOKEN 替换为自己的 Token：

```sh .npmrc
@mubeisama:registry=https://gitlab.com/api/v4/npm/

//gitlab.com/api/v4/packages/npm/:_authToken=<your_token>

//gitlab.com/api/v4/projects/31255575/packages/npm/:_authToken=<your_token>
```

修改版本号后执行发布命令即可

```sh
npm publish
```

![图 4](https://i.loli.net/2021/11/12/2zwCkZJTmAMbKS9.png)  

记录里会标注出是手动发布

### 端点的解释

点击发布的 npm 包进入详情，其中的 Registry setup 就是本文开头提到的包的端点

![图 4](https://i.loli.net/2021/11/12/yFn34tMJk1TeVxL.png)  

项目级和实例级的区别就在于安装地址不同：

- 项目级 下载地址必须是带上项目 ID 的地址
- 实例级  gitlab 的全局地址，更接近我们平时使用的 npm 注册表

那是所有发布的包都支持项目级和实例级端点吗？不是

前文中提到需要使用自己的用户名（根命名空间）作为 scope，这其实不是必须的，你可以使用任意字符作为 scope，任意 scope 的话（非根命名空间），这个包在安装时就只支持项目级端点，因为不符合[包命名约定](https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention)，使用实例级安装地址会报 404 error

而在发布包时，必须是项目级端点，也就是必须提供一个仓库，作为发布地址

## 使用包

随处找个地创建一个新文件夹

```sh
mkdir ues-hex2rgb && cd ues-hex2rgb
```

初始化项目，全默认

```sh
npm init
```

执行上图 Registry setup 的命令

```sh
echo @mubeisama:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

如果是公开的仓库，执行安装命令即可

```sh
npm i @mubeisama/hex2rgb
```

如果是私有仓库，还需要编辑 `.npmrc`，添加鉴权配置，再执行安装命令

```sh .npmrc
@mubeisama:registry=https://gitlab.com/api/v4/packages/npm/
# add auth token
//gitlab.com/api/v4/packages/npm/:_authToken=<your_token>
```

添加 `/app.js`

```js
const hex2rgb = require('@mubeisama/hex2rgb')

const rgb = hex2rgb('#dd2b0e')

console.log('rbg：', rgb)
```

执行

```sh
node app.js
# rbg： rbg(221, 43, 14)
```

打印正常，收工

## 参考链接

官方文档：<https://docs.gitlab.com/ee/user/packages/npm_registry/>

项目地址：<https://gitlab.com/MuBeiSAMA/hex2rgb>
